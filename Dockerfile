FROM python:3.11

WORKDIR /streamlit

COPY streamlit .

RUN pip install --no-cache-dir -r requirements.txt

EXPOSE 8501

CMD ["streamlit", "run", "Overview.py"]

