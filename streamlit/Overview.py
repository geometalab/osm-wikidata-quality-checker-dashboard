from datetime import datetime as dt

import data
import osmose
import pandas as pd
import plotly.express as px

import streamlit as st

st.set_page_config(
    page_title="Overview - OSM Wikidata Quality Checker Dashboard",
    page_icon="./owqc_favicon1.png",
    # layout="wide",
    # initial_sidebar_state="auto",
    # menu_items=None,
)

summary_df = data.get_summaries_df()
logs = data.get_logs()

# Body

st.title("OSM Wikidata Quality Checker")

if len(summary_df) == 0 or len(logs) == 0:
    st.error(f"Error: No runs found. This should not happen :(")
    st.stop()

st.write('This is the frontend of the [OSM Wikidata Quality Checker](https://gitlab.com/geometalab/osm-wikidata-quality-checker). Find all Infos in the README')
st.write('All issues get reported to [Osmose](https://osmose.openstreetmap.fr/en/map/). See the Issues on Osmose:')
st.link_button("Osmose Map", "https://osmose.openstreetmap.fr/en/map/#zoom=13&lat=47.2266&lon=8.8164&item=xxxx&level=1%2C2%2C3&tags=wikidata")

## Last Run
st.write("## Last Run Log")
last_log_key = list(logs.keys())[-1]
last_log_df = logs[last_log_key]
st.write(f"**Instance Name:** {data.get_log_instance_name(last_log_df)}")
st.write(f"**Start Time:** {last_log_df.index[0].split(',')[0]}")
st.write(f"**End/Last Time:** {last_log_df.index[-1].split(',')[0]}")
try:
    st.write(f"**Duration:** {pd.to_datetime(last_log_df.index[-1]) - pd.to_datetime(last_log_df.index[0])}")
except Exception as e:
    st.write(f"**Duration:** ERROR ({e})")
st.write(f"**Last Line:** {last_log_df.iloc[-1].message}")
# st.write(f"Last run duration: {last_run_df.index[-1].split(',')[0]}") # TODO
if st.checkbox("Show full log"):
    st.write("**Full Log:**")
    log_level = st.selectbox('Log Level', ['(ALL)', 'DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'])
    if log_level == '(ALL)':
        st.dataframe(last_log_df)
    else:
        st.dataframe(last_log_df[last_log_df['level'] == log_level])


## Osmose Update
st.write("## Last Osmose Update")
try:
    with st.spinner("Loading Osmose Update.."):
        update_since = osmose.get_last_update()
    st.write(f"**Osmose last update:** {update_since} days ago")
    # days since last run:
    last_run_end = dt.strptime(last_log_df.index[-1].split(',')[0], '%Y-%m-%d %H:%M:%S')
    days_since_last_run = (dt.now() - last_run_end).total_seconds() / (60 * 60 * 24)
    if abs(days_since_last_run - update_since)>1.5:
        st.warning(f"WARNING: Days since last run ({days_since_last_run:.1f}) and days since last Osmose update ({update_since:.1f}) differ.", icon="⚠️")
except Exception as e:
    st.error(f"Error: Could not get Osmose update: {e}")
    if st.button("Retry", type='primary'):
        st.rerun()
st.link_button("Osmose Update Matrix", "https://osmose.openstreetmap.fr/de/control/update_matrix")


## Last Issue Summary
st.write("## Last Issue Summary")
last_summary_df = summary_df.iloc[-1]
if last_summary_df.name != last_log_key:
    st.warning(f"WARNING: Last run log ({last_log_key}) has not the same date as last runs summary ({last_summary_df.name}). Therefore the last run is still in progress or has failed", icon="⚠️")
st.table(last_summary_df)
st.link_button("Osmose Issue List", "https://osmose.openstreetmap.fr/en/issues/done?country=&item=3031&source=&class=&username=&bbox=")

## Found issues over time
st.write("## Found issues over time")
# st.line_chart(summary_df)
pd.options.plotting.backend = "plotly"
fig = summary_df.plot()
st.plotly_chart(fig)

## Runtime over time
st.write("## Total runtime over time")
runtime_df = pd.DataFrame()
for name, df in logs.items():
    try:
        start = pd.to_datetime(df.index[0])
        end = pd.to_datetime(df.index[-1])
        duration = end - start
        runtime_df.loc[name, "start"] = start
        runtime_df.loc[name, "end"] = end
        runtime_df.loc[name, "duration_str"] = str(duration)
        runtime_df.loc[name, "duration_seconds"] = duration.total_seconds()
        runtime_df.loc[name, "duration_days"] = duration.total_seconds() / (60*60*24)
        runtime_df.loc[name, "success"] = data.check_log_success(df)
        runtime_df.loc[name, "instance_name"] = data.get_log_instance_name(df)
    except Exception as e:
        # st.warning(f"Error: Could not calculate runtime for {name}: {e}")
        runtime_df.loc[name, "start"] = float("nan")
        runtime_df.loc[name, "end"] = float("nan")
        runtime_df.loc[name, "duration_str"] = str(e)
        runtime_df.loc[name, "duration_seconds"] = 0
        runtime_df.loc[name, "duration_days"] = 0
        runtime_df.loc[name, "success"] = False
        runtime_df.loc[name, "instance_name"] = None
selected_instance_name = st.selectbox('Instance name', ["(ALL)"]+list(runtime_df['instance_name'].unique()))
if selected_instance_name != "(ALL)":
    runtime_df = runtime_df[runtime_df['instance_name'] == selected_instance_name]
st.plotly_chart(px.bar(data_frame=runtime_df, x=runtime_df.index, y="duration_days", color="success", title="Total runtime in days (24h)"))
if st.checkbox("Show runtime table"):
    st.dataframe(runtime_df)