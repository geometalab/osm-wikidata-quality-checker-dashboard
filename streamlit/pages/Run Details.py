import data
import pandas as pd

import streamlit as st

st.set_page_config(
    page_title="Run Details - OSM Wikidata Quality Checker Dashboard",
)

# @st.cache_data
# def cached_summary_df():
#     return data.get_summaries_df()


# @st.cache_data
# def cached_logs():
#     return data.get_logs()

# summary_df = cached_summary_df()
# logs = cached_logs()

summary_df = data.get_summaries_df()
logs = data.get_logs()

log_keys = list(logs.keys())

# Sidebar
format_run_name = lambda i: f"{log_keys[i]} {data.get_log_instance_name(logs[log_keys[i]])} {'' if data.check_log_success(logs[log_keys[i]]) else '❌'}"
with st.sidebar:
    selected_run = st.selectbox(label='Choose a run:', options=list(reversed(range(len(log_keys)))), format_func=format_run_name, index=0)
    # st.write(f"Selected run: {selected_run}")

# Header
st.title(f"Run Detail: {log_keys[selected_run]}")

## Navigation # TODO
# def change_run(offset):
#     global selected_run
#     selected_run += offset
# st.button('Previous', key='prev', on_click=change_run(-1))
# st.button('Next', key='next', on_click=change_run(+1))

## Body

st.subheader('Overview')
log_df = logs[log_keys[selected_run]]
st.write(f"**Instance Name:** {data.get_log_instance_name(log_df)}")
st.write(f"**Start Time:** {log_df.index[0].split(',')[0]}")
st.write(f"**End Time:** {log_df.index[-1].split(',')[0]}")
try:
    st.write(f"**Duration:** {pd.to_datetime(log_df.index[-1]) - pd.to_datetime(log_df.index[0])}")
except Exception as e:
    st.write(f"**Duration:** ERROR ({e})")
st.write(f"**Last Line:** {log_df.iloc[-1].message}")

st.subheader('Issue Summary')
try:
    st.table(summary_df.loc[log_keys[selected_run]])
    # st.bar_chart(summary_df.loc[log_keys[selected_run]])
except KeyError:
    st.error('ERROR: No summary found')

st.subheader('Log')
log_level = st.selectbox('Log Level', ['(ALL)', 'DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'])
if log_level == '(ALL)':
    st.dataframe(log_df)
else:
    st.dataframe(log_df[log_df['level'] == log_level])

# TODO provide reports for download


