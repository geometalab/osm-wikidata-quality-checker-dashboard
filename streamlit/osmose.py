# Osmose API
import requests


def get_last_update():
    res = requests.get('https://osmose.openstreetmap.fr/control/update_matrix.json', timeout=30)
    if res.status_code != 200:
        raise Exception(f"Osmose API status error: {res.status_code} {res.reason}")
    data = res.json()
    update_sinces = [v[0] for v in list(data['matrix']['Wikidata'].values())]
    return max(update_sinces)

