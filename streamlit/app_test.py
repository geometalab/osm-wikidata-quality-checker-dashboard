import os
from streamlit.testing.v1 import AppTest

APP_PATH = "Overview.py"

# os.chdir("streamlit")

# os.environ["LOGS_PATH"] = "sample_data/logs"
# os.environ["SUMMARIES_PATH"] = "sample_data/reports"
at = AppTest.from_file(APP_PATH)

# test Overview
at.run(timeout=35) # Because of the Osmose API call
for e in at.exception:
    print(e)
assert not at.exception

# test Run Details
at.switch_page("pages/Run Details.py")
at.run()
for e in at.exception:
    print(e)
assert not at.exception

print("All tests passed.")