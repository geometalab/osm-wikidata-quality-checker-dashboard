# reads the data folder and provides it

import json
import os
from pathlib import Path

import pandas as pd

SUMMARIES_PATH = os.getenv("SUMMARIES_PATH","../sample_data/reports")
LOGS_PATH = os.getenv("LOGS_PATH","../sample_data/logs")

# print(Path.cwd())
# print("SUMMARIES_PATH", SUMMARIES_PATH, Path(SUMMARIES_PATH).exists())
# print("LOGS_PATH", LOGS_PATH, Path(LOGS_PATH).exists())
assert Path(SUMMARIES_PATH).exists(), f"SUMMARIES_PATH does not exist: {SUMMARIES_PATH}"
assert Path(LOGS_PATH).exists(), f"LOGS_PATH does not exist: {LOGS_PATH}"

def _get_summaries_json(path):
    path = Path(path)
    summaries = []
    # get
    for f in path.glob("summaryreport_*.json"):
        fh = f.open()
        summaries.append((f.name, json.load(fh)))
        fh.close()
    return sorted(summaries)

def get_summaries_df(path=SUMMARIES_PATH):
    summaries = _get_summaries_json(path)
    if len(summaries)==0:
        return pd.DataFrame()
    names, jsons = zip(*summaries)
    dates = [n.split("_")[1] for n in names]
    categories = [x["categories"] for x in jsons]
    totals = [x["total"] for x in jsons]
    df = pd.concat(
        [
            pd.DataFrame(totals, index=dates, columns=["Total"]),
            pd.DataFrame(categories, index=dates),
        ],
        axis=1,
    )
    # df.index = pd.to_datetime(df.index)
    df.sort_index(inplace=True)
    return df

def get_logs(path=LOGS_PATH) -> dict[str, pd.DataFrame]:
    path = Path(path)
    logs = dict()
    logs_paths = sorted(list(path.glob("log_*.log")))
    for f in logs_paths:
        date = f.name.split('_')[1]
        # text = f.read_text()
        # df = pd.read_fwf(f, header=None,
        #                  colspecs=[(1, 24), (27, 31), (34, 45), (49, None)],
        #                  names=['time', 'level', 'process', 'message'],
        #                  index_col=0)
        df = pd.read_table(
            f,
            sep="\]",
            header=None,
            names=["timestamp", "level", "process", "message"],
            on_bad_lines="skip",
            engine="python",
        )
        for col in df.columns:
            df[col] = df[col].str.lstrip(" [-")
        df.set_index("timestamp", inplace=True)
        logs[date] = df
    return logs

# def get_log_info(df) -> dict:

def get_log_instance_name(df) -> str|None:
    try:
        return df.iloc[:40]["message"].str.extract(r"INSTANCE_NAME: '(.*)'")[0].dropna().iloc[0]
    except Exception as e:
        print("*********************")
        print(e)
        print("*********************")
        return None

def check_log_success(df) -> bool:
    # TODO the "alert sent" part can be removed in the future
    return df.iloc[-2].message == "alert sent: End run" or df.iloc[-2].message == "End run successfully" 

def get_log_duration(df) -> pd.Timedelta | None:
    try:
        return pd.to_datetime(df.index[-1]) - pd.to_datetime(df.index[0])
    except:
        return None