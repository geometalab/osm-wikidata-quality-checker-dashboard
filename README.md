# OpenStreetMap Wikidata Quality Checker Dashboard

showing the status and runs of the [OpenStreetMap Wikidata Quality Checker](https://gitlab.com/geometalab/osm-wikidata-quality-checker)

deployed under [osm-wikidata-quality-checker.data-processing.infs.ch](https://osm-wikidata-quality-checker.data-processing.infs.ch/)

build with [Streamlit](https://docs.streamlit.io/)

## Local development

- `pip install -r streamlit/requirements.tx`
- `cd streamlit && streamlit run Overview.py`

- The environment variables `LOGS_PATH` and `SUMMARIES_PATH` are per default to `sample_data` for development

## Deployment
s
- see `docker-compose.yaml`